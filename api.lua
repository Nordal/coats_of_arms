--[[

    Define your own coats_of_arms. Fill in the table with
    appropriate values.
    The first value is the pattern of the coat of arms. Pattern
    can be vertical, horizontal, slash.
    The second and third values define the colors the pattern is filled with.
    The names of the colors must be the same names, unifieddyes uses for the
    colors it provides. You can choose from the extended palette of 256 colors.



]]

--coats_of_arms.definition_table = {}

coats_of_arms.definition_table = {

--examples

{"vertical_divided", "medium_blue", "amber"},
{"horizontal_divided", "medium_blue", "amber"},
{"diagonal_divided", "medium_blue", "amber"},
{"diagonal_divided_backwards", "medium_blue", "amber"},
{"chequered", "medium_blue", "amber"},
{"cross_big", "medium_blue", "amber"},
{"cross_thin", "medium_blue", "amber"},
{"cross_diagonal", "medium_blue", "amber"},
{"diagonal", "medium_blue", "amber"},
{"diagonal_backwards", "medium_blue", "amber"},
{"line_vertical_big", "medium_blue", "amber"},
{"lines_vertical_three", "medium_blue", "amber"},
{"lines_Y_flipped", "medium_blue", "amber"},

--your definitions

}
--print("coats_of_arms.definition_table:")
--print(dump(coats_of_arms.definition_table))
