
coats_of_arms = {}

dofile(minetest.get_modpath("coats_of_arms").."/functions.lua")
dofile(minetest.get_modpath("coats_of_arms").."/api.lua")
dofile(minetest.get_modpath("coats_of_arms").."/nodes.lua")



minetest.log('action', '[Mod] Coats of Arms [v1.1.0] loaded.')

--[[
coats_of_arms.register_coat_of_arms = function(name, descr, pattern, col1, col2)
    print("register coat of arms:")
    print("params: "..name, descr, pattern, col1, col2)

	local front
    local mask
    local reci

	local c1 = valid_color(col1)
    print("col1 = "..tostring(c1))
    local c2 = valid_color(col2)
    print("col2 = "..tostring(c2))
    if not c1 or not c2 then
        return
    end
	--if (not valid_color(col1)) or (not valid_color(col2)) then
	--	return
	--end

	mask = valid_pattern(pattern)
	if not mask then
		return
	end
    print("mask = "..mask)

	front = col2..".png^("..col1..".png^[mask:"..mask..")"
	print("front = "..front)

	reci = receipt(pattern, col1, col2)
    print(dump(reci))
	if not reci then
		return
	end
end


-- register coats of arms defined in definition table (api.lua)
print("start loop through definition table")
for idx, field in ipairs(coats_of_arms.definition_table) do

    print(idx, field[1],field[2],field[3])

    local name = "coats_of_arms:"..field[1].."_"..field[2].."_"..field[3]
    print("name = "..name)

    local descr = S("Coats of Arms "..field[1].." "..field[2].." "..field[3])
    print("descr = "..descr)

    coats_of_arms.register_coat_of_arms(name,descr,field[1],field[2],field[3])
end


minetest.log('action', '[Mod] Coats of Arms [v1.1.0] loaded.')
]]
