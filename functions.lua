--coats_of_arms={}

coats_of_arms.valid_color = function (color)
	local colors = {}
	colors["amber_s50"] = true
	colors["amber"] = true
	colors["azure_s50"] = true
	colors["azure"] = true
	colors["black"] = true
	colors["blue_s50"] = true
	colors["blue"] = true
	colors["bright_amber"] = true
	colors["bright_azure"] = true
	colors["bright_blue"] = true
	colors["bright_cerulean"] = true
	colors["bright_chartreuse"] = true
	colors["bright_crimson"] = true
	colors["bright_cyan"] = true
	colors["bright_fuchsia"] = true
	colors["bright_green"] = true
	colors["bright_grey"] = true
	colors["bright_harlequin"] = true
	colors["bright_indigo"] = true
	colors["bright_lime"] = true
	colors["bright_magenta"] = true
	colors["bright_mulberry"] = true
	colors["bright_orange"] = true
	colors["bright_red"] = true
	colors["bright_rose"] = true
	colors["bright_sapphire"] = true
	colors["bright_spring"] = true
	colors["bright_turquoise"] = true
	colors["bright_vermilion"] = true
	colors["bright_violet"] = true
	colors["bright_yellow"] = true
	colors["cerulean_s50"] = true
	colors["cerulean"] = true
	colors["chartreuse_s50"] = true
	colors["chartreuse"] = true
	colors["crimson_s50"] = true
	colors["crimson"] = true
	colors["cyan_s50"] = true
	colors["cyan"] = true
	colors["dark_amber"] = true
	colors["dark_azure"] = true
	colors["dark_blue"] = true
	colors["dark_cerulean"] = true
	colors["dark_chartreuse"] = true
	colors["dark_crimson"] = true
	colors["dark_cyan"] = true
	colors["dark_fuchsia"] = true
	colors["dark_green"] = true
	colors["dark_grey"] = true
	colors["dark_harlequin"] = true
	colors["dark_indigo"] = true
	colors["dark_lime"] = true
	colors["dark_magenta"] = true
	colors["dark_mulberry"] = true
	colors["dark_orange"] = true
	colors["dark_red"] = true
	colors["dark_rose"] = true
	colors["dark_sapphire"] = true
	colors["dark_spring"] = true
	colors["dark_turquoise"] = true
	colors["dark_vermilion"] = true
	colors["dark_violet"] = true
	colors["dark_yellow"] = true
	colors["faint_amber"] = true
	colors["faint_azure"] = true
	colors["faint_blue"] = true
	colors["faint_cerulean"] = true
	colors["faint_chartreuse"] = true
	colors["faint_crimson"] = true
	colors["faint_cyan"] = true
	colors["faint_fuchsia"] = true
	colors["faint_green"] = true
	colors["faint_harlequin"] = true
	colors["faint_indigo"] = true
	colors["faint_lime"] = true
	colors["faint_magenta"] = true
	colors["faint_malachite"] = true
	colors["faint_mulberry"] = true
	colors["faint_orange"] = true
	colors["faint_red"] = true
	colors["faint_rose"] = true
	colors["faint_sapphire"] = true
	colors["faint_spring"] = true
	colors["faint_turquoise"] = true
	colors["faint_vernillon"] = true
	colors["faint_violet"] = true
	colors["faint_yellow"] = true
	colors["green_s50"] = true
	colors["green"] = true
	colors["grey"] = true
	colors["harlequin_s50"] = true
	colors["harlequin"] = true
	colors["indigo_s50"] = true
	colors["indigo"] = true
	colors["light_amber"] = true
	colors["light_azure"] = true
	colors["light_blue"] = true
	colors["light_cerulean"] = true
	colors["light_chartreuse"] = true
	colors["light_crimson"] = true
	colors["light_cyan"] = true
	colors["light_fuchsia"] = true
	colors["light_green"] = true
	colors["light_grey"] = true
	colors["light_harlequin"] = true
	colors["light_indigo"] = true
	colors["light_lime"] = true
	colors["light_magenta"] = true
	colors["light_malachite"] = true
	colors["light_mulberry"] = true
	colors["light_orange"] = true
	colors["light_red"] = true
	colors["light_rose"] = true
	colors["light_sapphire"] = true
	colors["light_spring"] = true
	colors["light_turquoise"] = true
	colors["light_vermilion"] = true
	colors["light_violet"] = true
	colors["light_yellow"] = true
	colors["lime_s50"] = true
	colors["lime"] = true
	colors["magenta_s50"] = true
	colors["magenta"] = true
	colors["malachite_s50"] = true
	colors["malachite"] = true
	colors["medium_amber"] = true
	colors["medium_azure"] = true
	colors["medium_blue"] = true
	colors["medium_cerulean"] = true
	colors["medium_chartreuse"] = true
	colors["medium_crimson"] = true
	colors["medium_cyan"] = true
	colors["medium_fuchsia"] = true
	colors["medium_green"] = true
	colors["medium_indigo"] = true
	colors["medium_lime"] = true
	colors["medium_magenta"] = true
	colors["medium_malachite"] = true
	colors["medium_mulberry"] = true
	colors["medium_orange"] = true
	colors["medium_red"] = true
	colors["medium_rose"] = true
	colors["medium_sapphire"] = true
	colors["medium_spring"] = true
	colors["medium_turquoise"] = true
	colors["medium_vermilion"] = true
	colors["medium_violet"] = true
	colors["medium_yellow"] = true
	colors["mulberry_s50"] = true
	colors["mulberry"] = true
	colors["orange_s50"] = true
	colors["orange"] = true
	colors["pastel_amber"] = true
	colors["pastel_azure"] = true
	colors["pastel_blue"] = true
	colors["pastel_cerulean"] = true
	colors["pastel_chartreuse"] = true
	colors["pastel_crimson"] = true
	colors["pastel_cyan"] = true
	colors["pastel_fuchsia"] = true
	colors["pastel_green"] = true
	colors["pastel_harlequin"] = true
	colors["pastel_indigo"] = true
	colors["pastel_lime"] = true
	colors["pastel_magenta"] = true
	colors["pastel_malachite"] = true
	colors["pastel_mulberry"] = true
	colors["pastel_orange"] = true
	colors["pastel_red"] = true
	colors["pastel_rose"] = true
	colors["pastel_sapphire"] = true
	colors["pastel_spring"] = true
	colors["pastel_turquoise"] = true
	colors["pastel_vernillon"] = true
	colors["pastel_violet"] = true
	colors["pastel_yellow"] = true
	colors["red_s50"] = true
	colors["red"] = true
	colors["rose_s50"] = true
	colors["rose"] = true
	colors["sapphire_s50"] = true
	colors["sapphire"] = true
	colors["spring_s50"] = true
	colors["spring"] = true
	colors["turquoise_s50"] = true
	colors["turquoise"] = true
	colors["vermilion_s50"] = true
	colors["vermilion"] = true
	colors["violet_s50"] = true
	colors["violet"] = true
	colors["white"] = true
	colors["yellow_s50"] = true
	colors["yellow"] = true

	if colors[color] then
		return true
    else
		return false
	end
end


coats_of_arms.patterns = function (pattern, col1, col2)
	local steel = "default:steel_ingot"
	local patts =
	{
		{"vertical_divided", "coats_of_arms_vertical_divided_mask.png",
		{
			{steel, steel, steel},
			{"unifieddyes:"..col1, steel, ""},
			{"unifieddyes:"..col2, ""   , ""}
		}},
		{"horizontal_divided", "coats_of_arms_horizontal_divided_mask.png",
		{
			{steel, steel, steel},
			{""                  , steel               , ""},
			{"unifieddyes:"..col1, "unifieddyes:"..col2, ""}
		}},
		{"diagonal_divided", "coats_of_arms_diagonal_divided_mask.png",
		{
			{steel, steel, steel},
			{""                  , steel, "unifieddyes:"..col2},
			{"unifieddyes:"..col1, ""   , ""}
		}},
		{"diagonal_divided_backwards", "coats_of_arms_diagonal_backwards_divided_mask.png",
		{
			{steel, steel, steel},
			{"unifieddyes:"..col1, steel, ""},
			{""                  ,    "", "unifieddyes:"..col2}
		}},
		{"chequered", "coats_of_arms_chequered_mask.png",
		{
			{steel, steel, steel},
			{"unifieddyes:"..col1, steel, "unifieddyes:"..col2},
			{"unifieddyes:"..col2, "unifieddyes:"..col1, ""}
		}},
		{"cross_big", "coats_of_arms_cross_big_mask.png",
		{
			{steel, steel, steel},
			{"unifieddyes:"..col1, steel, "unifieddyes:"..col2},
			{"unifieddyes:"..col1,    "", "unifieddyes:"..col2}
		}},
		{"cross_thin", "coats_of_arms_cross_thin_mask.png",
		{
			{steel, steel, steel},
			{"unifieddyes:"..col1, steel, "unifieddyes:"..col2},
			{""                  , ""   , ""}
		}},
		{"cross_diagonal", "coats_of_arms_cross_diagonal_mask.png",
		{
			{steel, steel, steel},
			{"unifieddyes:"..col1, steel, "unifieddyes:"..col2},
			{"unifieddyes:"..col2,    "", "unifieddyes:"..col1}
		}},
		{"diagonal", "coats_of_arms_diagonal_mask.png",
		{
			{steel, steel, steel},
			{""                  , "unifieddyes:"..col2, steel},
			{"unifieddyes:"..col1, ""                  , ""   }
		}},
		{"diagonal_backwards", "coats_of_arms_diagonal_backwards_mask.png",
		{
			{steel, steel, steel},
			{"unifieddyes:"..col1, ""                  , steel},
			{""                  , "unifieddyes:"..col2, ""   }
		}},
		{"line_vertical_big", "coats_of_arms_line_vertical_big_mask.png",
		{
			{steel, steel, steel},
			{"unifieddyes:"..col1, "", steel},
			{"unifieddyes:"..col2, "", ""   }
		}},
		{"lines_vertical_three", "coats_of_arms_lines_vertical_three_mask.png",
		{
			{steel, steel, steel},
			{"unifieddyes:"..col1, "unifieddyes:"..col2, steel},
			{"unifieddyes:"..col1, ""                  , ""   }
		}},
		{"lines_Y_flipped", "coats_of_arms_lines_Y_flipped_mask.png",
		{
			{steel, steel, steel},
			{"unifieddyes:"..col1, "unifieddyes:"..col2, steel},
			{"unifieddyes:"..col2, ""   , "unifieddyes:"..col2}
		}},
	}

	for idx, val in ipairs(patts) do
		if val[1] == pattern then
			return {val[2], val[3]} --returns mask, recipe
		end
	end
end

--local res = coats_of_arms.patterns("lines_vertical_three", "cyan", "medium_magenta")
