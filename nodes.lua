
-- get variables for definition of node and craft recipe

coats_of_arms.nodes = {}
local check = true
local mask_recipe

for idx, field in ipairs(coats_of_arms.definition_table) do

    def = {}

    -- get pattern and colors from definition table (api.lua)

    def.pattern = field[1]
    def.col1 = field[2]
    def.col2 = field[3]

    -- construct name and description

    def.name = "coats_of_arms:"..field[1].."_"..field[2].."_"..field[3]
    def.descr = "Coats of Arms "..field[1].." "..field[2].." "..field[3]

    -- check if colors exist

    if not coats_of_arms.valid_color(def.col1) then
        check = false
    end

	if not coats_of_arms.valid_color(def.col2) then
        check = false
    end

    -- check pattern, get mask and recipe of the pattern

    mask_recipe = coats_of_arms.patterns(def.pattern, def.col1, def.col2)
    if not mask_recipe then
        check = false
    else
        def.mask = mask_recipe[1]
        def.recipe = mask_recipe[2]
    end

    if not check then

        -- skip inserting def values and set check back to true
        check = true
    else

        -- construct front tile out of colors and mask
        def.front = def.col2..".png^("..def.col1..".png^[mask:"..def.mask..")"

        -- insert def values into node definition table
        table.insert(coats_of_arms.nodes, def)
    end

end


-- function for registering nodes

coats_of_arms.register_nodes = function(def)
    minetest.register_node(def.name, {
    	description = def.descr,
    	drawtype = "nodebox",
    	--up (+Y), down (-Y), right (+X), left (-X), back (+Z), front (-Z).
    	tiles = {
    		"default_steel_block.png",
    		"default_steel_block.png",
    		"default_steel_block.png",
    		"default_steel_block.png",
    		"default_steel_block.png",
    		def.front
    	},
    	paramtype = "light",
    	paramtype2 = "facedir",
    	is_ground_content = false,
    	groups = {cracky = 3, oddly_breakable_by_hand = 2},
    	sounds = default.node_sound_metal_defaults(),
    	node_box = {
    		type = "fixed",
    		fixed = {
    			{-0.0625, -0.5, 0.25, 0.0625, -0.46875, 0.5}, -- NodeBox1
    			{-0.09375, -0.46875, 0.25, 0.09375, -0.4375, 0.5}, -- NodeBox2
    			{-0.1875, -0.4375, 0.25, 0.1875, -0.40625, 0.5}, -- NodeBox3
    			{-0.28125, -0.40625, 0.25, 0.28125, -0.375, 0.5}, -- NodeBox4
    			{-0.34375, -0.375, 0.25, 0.34375, -0.34375, 0.5}, -- NodeBox5
    			{-0.375, -0.34375, 0.25, 0.375, -0.3125, 0.5}, -- NodeBox6
    			{-0.40625, -0.3125, 0.25, 0.40625, -0.28125, 0.5}, -- NodeBox7
    			{-0.40625, -0.28125, 0.25, 0.40625, -0.25, 0.5}, -- NodeBox8
    			{-0.4375, -0.25, 0.25, 0.4375, -0.21875, 0.5}, -- NodeBox9
    			{-0.4375, -0.21875, 0.25, 0.4375, -0.1875, 0.5}, -- NodeBox10
    			{-0.46875, -0.1875, 0.25, 0.46875, -0.15625, 0.5}, -- NodeBox11
    			{-0.46875, -0.15625, 0.25, 0.46875, -0.125, 0.5}, -- NodeBox12
    			{-0.46875, -0.125, 0.25, 0.46875, -0.09375, 0.5}, -- NodeBox13
    			{-0.5, -0.09375, 0.25, 0.5, -0.0625, 0.5}, -- NodeBox14
    			{-0.5, -0.0625, 0.25, 0.5, 0.5, 0.5}, -- NodeBox15
    			}
    		},
    selection_box = {
    		type = "fixed",
    		fixed = {
    			{-0.500000,-0.500000,0.25000,0.500000,0.500000,0.500000},
    		},
    	},
    })
end


-- function for registering craft recipes

coats_of_arms.register_recipes = function(def)
    minetest.register_craft({
    	output = "coats_of_arms:"..def.pattern,
    	recipe = def.recipe,
    })
end


--register nodes

for idx, val in ipairs(coats_of_arms.nodes) do
    coats_of_arms.register_nodes(val)
end


-- register craft recipe

for idx, val in ipairs(coats_of_arms.nodes) do
    coats_of_arms.register_recipes(val)
end
